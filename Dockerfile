FROM ubuntu:latest
RUN apt-get update && apt-get install -y curl && \
    curl -fsSL https://deb.nodesource.com/setup_18.x | bash - && \
    apt-get install -y nodejs sudo apt-utils && \
    npm install -g npm@9.6
WORKDIR /app
COPY . /app
RUN npm install
EXPOSE 7700
CMD ["node", "-v"]
ENTRYPOINT ["node", "service/email.js"]
